# PhyPID Meshes

This contains [Gmsh](https://gmsh.info) geometry source files (`*.geo*`) for some meshes. 

To visualize a mesh, load it in Gmsh and press the `3` key to create a 3D mesh.

``` console
$ gmsh rods.geo
```

Use `Tools -> Visibility` (Control-Shift-V) to inspect entities and physical groups (the labels used by analysis).
