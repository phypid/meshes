DefineConstant[
  angle = {60, Min 1, Max 360, Step 1,
    Name "Parameters/Angle"}
];

DefineConstant[
  arcres = {30, Min 1, Max 90, Step 1,
    Name "Parameters/Arc Resolution"}
];

DefineConstant[
  radres = {.5, Min .01, Max 1, Step .01,
    Name "Parameters/Radial Resolution"}
];

DefineConstant[
  height = {.5, Min .1, Max 10, Step .1,
    Name "Parameters/Height"}
];

DefineConstant[
  heightres = {.5, Min .1, Max height, Step .1,
    Name "Parameters/Height Resolution"}
];

Rinner = 1;
Router = 2;
Point(1) = {1, 0, 0};
Point(2) = {2, 0, 0};
Line(1) = {1, 2};
Characteristic Length {:} = radres;
Extrude {{0, 0, 1}, {0,0,0}, angle*Pi/180} { Line{1}; Layers{Ceil(angle/arcres)}; Recombine; }
Extrude {0, 0, height} { Surface{:}; Layers{Ceil(height/heightres)}; Recombine; }

Physical Surface("bottom") = {5};
Physical Surface("start") = {14};
Physical Surface("outer") = {18};
Physical Surface("end") = {22};
Physical Surface("inner") = {26};
Physical Surface("top") = {27};
Physical Volume("domain") = {1};

Mesh.ElementOrder = 2;
Mesh.SecondOrderIncomplete = 1;
