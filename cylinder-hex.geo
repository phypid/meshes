// Gmsh project created on Fri May 05 15:18:55 2023
SetFactory("OpenCASCADE");
DefineConstant[
  length = {5, Min 1, Max 100, Step 1, Name "Parameters/length"},
  diameter = {5, Min 1, Max 20, Step 1, Name "Parameters/diameter"},
  extrude_layers = {10, Min 1, Max 100, Step 1, Name "Parameters/extrusion layers"},
  radial_elements = {4, Min 1, Max 100, Step 1, Name "Parameters/radial elements"},
  arc_elements = {8, Min 1, Max 100, Step 1, Name "Parameters/arc elements"}
];

r = diameter / 2;

// Points
Point(1) = {0, 0, 0, 1.0};
Point(2) = {r, 0, 0, 1.0};
Point(3) = {0, r, 0, 1.0};
Point(4) = {-r, 0, 0, 1.0};
Point(5) = {0, -r, 0, 1.0};
Point(6) = {r/2, 0, 0, 1.0};
Point(7) = {0, r/2, 0, 1.0};
Point(8) = {-r/2, 0, 0, 1.0};
Point(9) = {0, -r/2, 0, 1.0};

// Curves
Circle(1) = {2, 1, 3};
Circle(2) = {3, 1, 4};
Circle(3) = {4, 1, 5};
Circle(4) = {5, 1, 2};
Line(5) = {2, 6};
Line(6) = {3, 7};
Line(7) = {4, 8};
Line(8) = {5, 9};
Line(9) = {6, 7};
Line(10) = {7, 8};
Line(11) = {8, 9};
Line(12) = {9, 6};

// Create loops
Line Loop(13) = {1, 6, -9, -5};
Line Loop(14) = {2, 7, -10, -6};
Line Loop(15) = {3, 8, -11, -7};
Line Loop(16) = {4, 5, -12, -8};
Line Loop(17) = {9, 10, 11, 12};

// Create Surfaces
Surface(18) = {13};
Surface(19) = {14};
Surface(20) = {15};
Surface(21) = {16};
Surface(22) = {17};

// Specify mesh intervals
Transfinite Curve {1} = arc_elements+1;
Transfinite Curve {2} = arc_elements+1;
Transfinite Curve {3} = arc_elements+1;
Transfinite Curve {4} = arc_elements+1;
Transfinite Curve {5} = radial_elements+1;
Transfinite Curve {6} = radial_elements+1;
Transfinite Curve {7} = radial_elements+1;
Transfinite Curve {8} = radial_elements+1;
Transfinite Curve {9} = arc_elements+1;
Transfinite Curve {10} = arc_elements+1;
Transfinite Curve {11} = arc_elements+1;
Transfinite Curve {12} = arc_elements+1;

Recombine Surface {18:22};

// Create 3D volume
Extrude {0,0,length} {Surface {18:22}; Layers{extrude_layers}; Recombine; }

Transfinite Surface "*";
Recombine Surface "*";

// Add top and bottom surfaces 
Physical Surface("top", 2) = {35, 38, 39, 31, 27};
Physical Surface("bottom", 1) = {20, 21, 18, 19, 22};
Physical Volume("elastic", 3) = {1, 2, 3, 4, 5};
