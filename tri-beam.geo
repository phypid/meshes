DefineConstant[
  h = {.25, Min .01, Max 1, Step .01, Name "Parameters/beam half-width"}
  extrude_length = {h, Min .1, Max 10, Step .1, Name "Parameters/extrusion length"}
  extrude_layers = {3, Min 1, Max 100, Step 1, Name "Parameters/extrusion layers"}
  length_elems = {10, Min 1, Max 100, Step 1, Name "Parameters/elements along length"}
  width_elems = {5, Min 1, Max 50, Step 1, Name "Parameters/elements along width"}
];

hi = h / Sin(Pi/3);

Point(1) = {0,0,0};
For i In {0:2}
    theta = 2*Pi*i/3;
    phi = theta + Pi/3;
    c = Cos(theta);
    s = Sin(theta);
    Point(10+i) = {c,s,0};  // axial
    Point(20+i) = {hi*Cos(phi), hi*Sin(phi), 0}; // inner diagonal
    Point(30+i) = {c+h*s, s-h*c, 0}; // outer behind
    Point(40+i) = {c-h*s, s+h*c, 0}; // outer ahead
EndFor
For i In {0:2}
    Line(10+i) = {1, 10+i};
    Line(20+i) = {10+i, 40+i};
    Line(30+i) = {40+i, 20+i};
    Line(40+i) = {20+i, 1};
    Line(50+i) = {20+(i+2)%3, 30+i};
    Line(60+i) = {30+i, 10+i};
    Transfinite Curve {10+i, 30+i, 50+i} = length_elems;
    Transfinite Curve {20+i, 40+i, 60+i} = width_elems;
EndFor
For i In {0:2}
    Curve Loop(10+i) = {10+i, 20+i, 30+i, 40+i};
    Plane Surface(10+i) = {10+i};
    Curve Loop(20+i) = {-(40+(i+2)%3), 50+i, 60+i, -(10+i)};
    Plane Surface(20+i) = {20+i};
EndFor

Transfinite Surface {:};
Recombine Surface {:};
body_extrusion[] = Extrude {0, 0, extrude_length} {Surface{:}; Layers{extrude_layers}; Recombine;};

Physical Volume("tribeam") = {1:6};
Physical Surface("right") = {75, 145};
Physical Surface("topleft") = {97, 167};
Physical Surface("bottomleft") = {119, 189};
