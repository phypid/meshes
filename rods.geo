SetFactory("OpenCASCADE");

radius = 0.3;
DefineConstant[
  nx = {3, Min 1, Max 30, Step 1, Name "Parameters/nx"}
  ny = {2, Min 1, Max 30, Step 1, Name "Parameters/ny"}
  extrude_length = {1, Min .1, Max 10, Step .1, Name "Parameters/extrusion length"}
  extrude_layers = {10, Min 1, Max 100, Step 1, Name "Parameters/extrusion layers"}
];
N = nx * ny;
Rectangle(1) = {0, 0, 0, 1, 1, 0};
Disk(10) = {0.5, 0.5, 0, radius};
BooleanFragments{Surface{1,10}; Delete;}{} // Creates 10 (rod) and 11 (binder)

For i In {0:nx-1}
  For j In {0:ny-1}
    If (i + j > 0)
       Translate {i, j, 0} { Duplicata { Surface{10:11}; } }
    EndIf
  EndFor
EndFor

Coherence;

// All the straight edges should have 8 elements
Transfinite Curve {:} = 8+1;

// Select the circles
circles = {};
For i In {0:nx-1}
  For j In {0:ny-1}
    lo = .5 - radius - 1e-4;
    hi = .5 + radius + 1e-4;
    circles() += Curve In BoundingBox {
      i + lo, j + lo, -1e-4,
      i + hi, j + hi, +1e-4
    };
  EndFor
EndFor

// Circles need 16 elements
Transfinite Curve {circles()} = 16+1;

Recombine Surface {10:10+2*N-1};
//Mesh.Algorithm = 8;
//Mesh.RecombinationAlgorithm = 3;

Extrude {0, 0, extrude_length} { Surface{10:10+2*N-1}; Layers{extrude_layers}; Recombine; }

e = 1e-4;
extrude_start() = Surface In BoundingBox {-e, -e, -e, nx+e, ny+e, e};
extrude_end() = Surface In BoundingBox {
  -e, -e, extrude_length-e,
  nx+e, ny+e, extrude_length+e};

Physical Surface("start") = {extrude_start()};
Physical Surface("end") = {extrude_end()};
Physical Volume("rod") = {1:2*N:2};
Physical Volume ("binder") = {2:2*N+1:2};
