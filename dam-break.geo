Lx = 100;
dx = 5;
Ly = 100;
y1 = 70;
y2 = -5;

// p1----l1-----p2  p3-----l5-----p4
// |            |   |             |
// |            l2  l4            |
// |            |   |             |
// |            p5--p6            |
// |                              |
// |                              |
// l12                            l6
// |            p7--p8            |
// |            |   |             |
// |            |   |             |
// |            l10 l8            |
// |            |   |             |
// |            |   |             |
// p9----l11----p10 p11-----l7----p12

Point(1) = {-Lx, Ly, 0};
Point(2) = {-dx, Ly, 0};
Point(3) = {dx, Ly, 0};
Point(4) = {Lx, Ly, 0};
Point(5) = {-dx, y1, 0};
Point(6) = {dx, y1, 0};
Point(7) = {-dx, y2, 0};
Point(8) = {dx, y2, 0};
Point(9) = {-Lx, -Ly, 0};
Point(10) = {-dx, -Ly, 0};
Point(11) = {dx, -Ly, 0};
Point(12) = {Lx, -Ly, 0};

Line(1) = {1, 2};
Line(2) = {2, 5};
Line(3) = {5, 6};
Line(4) = {6, 3};
Line(5) = {3, 4};
Line(6) = {4, 12};
Line(7) = {12, 11};
Line(8) = {11, 8};
Line(9) = {8, 7};
Line(10) = {7, 10};
Line(11) = {10, 9};
Line(12) = {9, 1};

Curve Loop(1) = {1:12};
Plane Surface(1) = {1};

Physical Curve("exterior", 1) = {1, 5, 6, 7, 11, 12};
Physical Curve("short_wall", 2) = {2:4};
Physical Curve("long_wall", 3) = {8:10};
Physical Surface(10) = {1};
