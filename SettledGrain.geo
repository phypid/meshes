SetFactory("Built-in");
Merge "../micromorph/share/stl2vox/SettledGrains/P1_B3D_Settled.stl";

DefineConstant[
  // Angle between two triangles above which an edge is considered as sharp
  angle = {60, Min 20, Max 120, Step 1,
    Name "Parameters/Angle for surface detection"},
  // For complex geometries, patches can be too complex, too elongated or too
  // large to be parametrized; setting the following option will force the
  // creation of patches that are amenable to reparametrization:
  forceParametrizablePatches = {0, Choices{0,1},
    Name "Parameters/Create surfaces guaranteed to be parametrizable"},
  // For open surfaces include the boundary edges in the classification process:
  includeBoundary = 0,
  // Force curves to be split on given angle:
  curveAngle = 180
];

// To split and classify
// ClassifySurfaces{angle * Pi/180, includeBoundary, forceParametrizablePatches,
//                  curveAngle * Pi / 180};
// CreateGeometry;
// Surface Loop(2) = {2, 3};
// Volume(2) = {2};

Surface Loop(1) = {1};
Volume(1) = {1};

// Can't combine OpenCASCADE with STL from built-in so we create the cylinder directly
// SetFactory("OpenCASCADE");
// Cylinder(10) = {0,0,-.02, 0,0,.14, .1};

radius = 0.1;
z0 = -.01;
height = 0.12;
Point(10) = {0, 0, z0};
Point(11) = {radius, 0, z0};
Point(12) = {0, radius, z0};
Point(13) = {-radius, 0, z0};
Point(14) = {0, -radius, z0};
Circle(1) = {11, 10, 12};
Circle(2) = {12, 10, 13};
Circle(3) = {13, 10, 14};
Circle(4) = {14, 10, 11};
Curve Loop(1) = {1, 2, 3, 4};
Plane Surface(10) = {1};
result[] = Extrude {0,0,height} {Surface{10};};

// Printf("%g %g %g %g %g", result[0], result[1], result[2], result[3], result[4]);
Surface Loop(10) = {10, result[0], result[2], result[3], result[4], result[5]};
Volume(10) = {10, 1};
Delete{ Volume{result[1]}; }

Mesh.Algorithm3D = 1;

Physical Volume("grain") = {1};
Physical Volume("binder") = {10};
Physical Surface("bottom") = {10};
Physical Surface("top") = {result[0]};
